package com.gwjvw.gbot;

import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateNicknameEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class GBotListener extends ListenerAdapter {
	private char disallowedCharacters[] = {'!', '?'};
	
	@Override
    public void onMessageReceived(MessageReceivedEvent event) {
    	if(event.getAuthor().getName().equals("GBot")) {
    		return;
    	}
    	System.out.println("Received message");
    	
    	String author = event.getAuthor().getName();
    	String mes = event.getMessage().getContentDisplay();
    	
    	//event.getChannel().sendMessage(author + ": " + mes).queue();
    }
	
	@Override
	public void onGuildMemberUpdateNickname(GuildMemberUpdateNicknameEvent event) {
		System.out.println("Somebody updated somezin");
		if(!isAllowed(event.getNewNickname().charAt(0))) {
			System.out.println(event.getMember().getEffectiveName() + ": " + event.getNewNickname());
			String id = event.getUser().getId();
			event.getGuild().kick(id, "Your name had an disallowed character at the beginning. Stop being an attention whore.");
		}
	}
	
	private boolean isAllowed(char character) {
		System.out.println(character);
		for(int i = 0; i < disallowedCharacters.length; i++) {
			if(character == disallowedCharacters[i]) {
				return false;
			}
		}
		return true;
	}
}
